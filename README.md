With the rise of automation there needs to be a method that will indicate when a device should be 
turned on and off instead of being left to run for hours. One method to solve this problem is with 
pressure sensors, where any variation of weight will send a signal to a microcontroller and 
allow devices to act accordingly. A HAT connected to the microcontroller will be used as an 
interface between the pressure sensor, pressure status LEDs, motor and microcontroller itself. 
This HAT could potentially be used in toys such as functioning model cranes that will turn on 
the motors when a load is detected on the cable attached to a strain gauge. It can also be 
utilised within devices such as blenders and similar light kitchen appliances. In this case, 
the weight sensing strain gauge can weigh the amount of produce inserted into the blender. 
With a heavier load, the motor will need to increase power. Another use for the HAT is to 
create an automatic water dispenser for a pet. Here, the water level in the pet’s bowl will need 
to be kept constant. As soon as it drops, a motor will trigger and a valve will turn, filling 
up the bowl to a set amount.

Bill of Materials
-Resistors
-Capacitors
-Op-amps
-Transistors
-Inductors
-Diodes
-MOSFETS
-LTC4359 IC
